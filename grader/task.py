import os
import os.path
import sys
import pathlib
import json
import re
import shutil
import subprocess
import codecs

from .sandbox import Sandbox


def copy_sources(submit_path, task_path, sources, check_fn=None):
    if not submit_path.exists():
        raise RuntimeError("Directory '{}' does not exists".format(submit_path))
    
    for pattern in sources:
        for file in submit_path.glob(pattern):
            if check_fn is not None:
                check_fn(file)

            target_path = task_path / file.relative_to(submit_path)
            target_path.parent.mkdir(parents=True, exist_ok=True)
            shutil.copyfile(file, target_path)


class Task:
    @classmethod
    def list(cls, root=pathlib.Path(".")):
        tasks = []
        for path in root.iterdir():
            if not path.is_dir():
                continue

            if not (path / ".tester.json").exists():
                continue

            tasks.append(Task(path.name, root))

        return tasks

    def __init__(self, name, root=None):
        self.name = name
        self.dry_run = False
        
        root = root or pathlib.Path('.')
        self.task_path = root / name
        self.task_private_path = root / 'private' / name

        with (self.task_path / ".tester.json").open() as f:
            config = json.load(f)

            self.sources = config["allow_change"]
            if not isinstance(self.sources, list):
                self.sources = [self.sources]

            self.tests = config.get("tests", [])
            self.benchmarks = config.get("benchmarks", [])

            self.regexp_ban = config.get("regexp_ban", [])

            self.review = config.get("review", False)

            self.disable_asan = config.get("disable_asan", False)
            self.disable_tsan = config.get("disable_tsan", False)


    def check_call(self, cmd, **kwargs):
        if self.dry_run:
            return

        sys.stderr.write("> " + " ".join(cmd) + "\n")
        sys.stderr.flush()
        subprocess.check_call(cmd, **kwargs)


    def build(self, build_type, test_solution=False):
        build_dir = str(self.task_path / "build" / build_type)

        self.check_call(["mkdir", "-p", build_dir])

        cmake_cmd = [
            "cmake", "../../",
            "-G", "Ninja",
            "-DCMAKE_BUILD_TYPE=" + build_type
        ]
        if test_solution:
            cmake_cmd.append("-DTEST_SOLUTION=YES")

        self.check_call(cmake_cmd, cwd=build_dir)

        self.check_call(["ninja", "-v", "-C", build_dir])

    @property
    def build_types(self):
        types = ["release"]
        if not self.disable_asan:
            types += ["asan"]
        if not self.disable_tsan:
            types += ["tsan"]
        return types

    def check_regexp_ban(self, file):
        try:
            file_content = codecs.open(file, encoding='utf-8').read()
        except UnicodeError as e:
            raise RuntimeError("File {} contains non-unicode characters".format(file)) from e

        for regexp in self.regexp_ban:
            if re.search(regexp, file_content, re.MULTILINE):
                raise RuntimeError("File {} contains banned regexp '{}'".format(file, regexp))

    def check_clang_format(self, file):
        replacements_xml = subprocess.check_output(["clang-format", "--output-replacements-xml", file])
        fixes_count = len(replacements_xml.split(b"\n"))
        fixes_limit = 10

        if fixes_count - 3 > fixes_limit:
            raise RuntimeError("clang-format found more than {} code formatting violations in {}. Please run all your code through clang-format.".format(fixes_limit, file))
    
    def copy_sources(self, submit_root):
        def check_fn(file):
            self.check_regexp_ban(file)
            self.check_clang_format(file)

        copy_sources(pathlib.Path(submit_root) / self.name, self.task_path, self.sources, check_fn)

    def grade(self, submit_root):
        self.copy_sources(submit_root)

        for build_type in self.build_types:
            self.build(build_type)

        for build_type in self.build_types:
            for test in self.tests:
                self.run_test(test, build_type)

            for benchmark in self.benchmarks:
                self.run_benchmark(test, build_type)

    def run_test(self, test, build_type, sandbox=True):
        self.check_call([str(self.task_path / "build" / build_type / test)])

    def run_benchmark(self, benchmark, build_type, sandbox=True):
        self.check_call([str(self.task_path / "build" / build_type / benchmark)])
        
    def check(self):
        for regex in self.regexp_ban:
            re.compile(regex)

        for src in self.sources:
            if not self.task_path.glob(src):
                raise ValueError("Source file '{}' not found in {}".format(src, name))

        for build_type in self.build_types:
            self.build(build_type, test_solution=True)

        for build_type in self.build_types:
            for test in self.tests:
                self.run_test(test, build_type, sandbox=False)

            for benchmark in self.benchmarks:
                self.run_benchmark(benchmark, build_type, sandbox=False)
