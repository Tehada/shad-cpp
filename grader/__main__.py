"""C++ Task Grader

Usage:
  grader
  grader pre-release-check [--task=<task>]

Options:
  -h --help    Show this message.
"""
import docopt


from .task import Task


def main():
    args = docopt.docopt(__doc__, version='C++ Task Grader 1.0')

    if args["pre-release-check"]:
        if args["--task"] is None:
            for task in Task.list():
                task.check()
        else:
            task = Task(args["--task"])
            task.check()
    else:
        task = Task.from_ci_env()
        task.grade()


if __name__ == "__main__":
    main()
